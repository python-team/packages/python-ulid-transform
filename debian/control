Source: python-ulid-transform
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Edward Betts <edward@4angle.com>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 pybuild-plugin-pyproject,
 python3-all,
 python3-poetry-core,
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-setuptools,
Rules-Requires-Root: no
Standards-Version: 4.7.1
Homepage: https://github.com/Bluetooth-Devices/ulid-transform
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-ulid-transform
Vcs-Git: https://salsa.debian.org/python-team/packages/python-ulid-transform.git

Package: python3-ulid-transform
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Fast ULID transformations
 Provides functionalities for creating and transforming ULIDs (Universally
 Unique Lexicographically Sortable Identifiers). Utilises a Cython-based
 implementation for optimal performance, with a fallback to pure Python if
 Cython is unavailable.
 .
 Example usage:
 .
  >>> import ulid_transform
  >>> ulid_transform.ulid_hex()
  '01869a2ea5fb0b43aa056293e47c0a35'
  >>> ulid_transform.ulid_now()
  '0001HZX0NW00GW0X476W5TVBFE'
  >>> ulid_transform.ulid_at_time(1234)
  '000000016JC62D620DGYNG2R8H'
  >>> ulid_transform.ulid_to_bytes('0001HZX0NW00GW0X476W5TVBFE')
  b'\x00\x00c\xfe\x82\xbc\x00!\xc0t\x877\x0b\xad\xad\xee'
